#!/bin/sh

./composer.phar install --optimize-autoloader
./composer.phar dump-autoload
chmod 0777 storage/*
sudo chmod 0777 storage/framework/*
php artisan config:clear
php artisan route:clear
php artisan clear-compiled
php artisan optimize --force
